/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */

package org.kathra.platform.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* KathraCatalogResourceParameterValidationRule
*/
@ApiModel(description = "KathraCatalogResourceParameterValidationRule")
public class KathraCatalogResourceParameterValidationRule extends KathraResource {

    /**
    * type
    */
    public enum TypeEnum {
        @SerializedName("empty")
        EMPTY("empty"),
        @SerializedName("alphaNum")
        ALPHANUM("alphaNum"),
        @SerializedName("minValue")
        MINVALUE("minValue"),
        @SerializedName("maxValue")
        MAXVALUE("maxValue"),
        @SerializedName("minLength")
        MINLENGTH("minLength"),
        @SerializedName("maxLength")
        MAXLENGTH("maxLength");

        private String value;

        TypeEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static TypeEnum fromValue(String text) {
            for (TypeEnum b : TypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected TypeEnum type = null;
    protected String value = null;
    protected String prompt = null;

    /**
    * Quick Set type
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameterValidationRule.type(TypeEnum).anotherQuickSetter(..))
    * @param TypeEnum type
    * @return KathraCatalogResourceParameterValidationRule The modified KathraCatalogResourceParameterValidationRule object
    **/
    public KathraCatalogResourceParameterValidationRule type(TypeEnum type) {
        this.type = type;
        return this;
    }

    /**
    * type
    * @return TypeEnum type
    **/
    @ApiModelProperty(value = "type")
    public TypeEnum getType() {
        return type;
    }

    /**
    * type
    **/
    public void setType(TypeEnum type) {
        this.type = type;
    }

    /**
    * Quick Set value
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameterValidationRule.value(String).anotherQuickSetter(..))
    * @param String value
    * @return KathraCatalogResourceParameterValidationRule The modified KathraCatalogResourceParameterValidationRule object
    **/
    public KathraCatalogResourceParameterValidationRule value(String value) {
        this.value = value;
        return this;
    }

    /**
    * value
    * @return String value
    **/
    @ApiModelProperty(value = "value")
    public String getValue() {
        return value;
    }

    /**
    * value
    **/
    public void setValue(String value) {
        this.value = value;
    }

    /**
    * Quick Set prompt
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameterValidationRule.prompt(String).anotherQuickSetter(..))
    * @param String prompt
    * @return KathraCatalogResourceParameterValidationRule The modified KathraCatalogResourceParameterValidationRule object
    **/
    public KathraCatalogResourceParameterValidationRule prompt(String prompt) {
        this.prompt = prompt;
        return this;
    }

    /**
    * prompt
    * @return String prompt
    **/
    @ApiModelProperty(value = "prompt")
    public String getPrompt() {
        return prompt;
    }

    /**
    * prompt
    **/
    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KathraCatalogResourceParameterValidationRule kathraCatalogResourceParameterValidationRule = (KathraCatalogResourceParameterValidationRule) o;
        return Objects.equals(this.type, kathraCatalogResourceParameterValidationRule.type) &&
        Objects.equals(this.value, kathraCatalogResourceParameterValidationRule.value) &&
        Objects.equals(this.prompt, kathraCatalogResourceParameterValidationRule.prompt) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, value, prompt, super.hashCode());
    }
}

