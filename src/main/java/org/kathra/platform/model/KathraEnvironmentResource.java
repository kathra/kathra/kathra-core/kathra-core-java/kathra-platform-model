/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */

package org.kathra.platform.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* KathraEnvironmentResource
*/
@ApiModel(description = "KathraEnvironmentResource")
public class KathraEnvironmentResource extends KathraResource {

    protected List<KathraEnvironmentResourceParameter> parameters = null;
    protected String catalogResource = null;

    /**
    * Quick Set parameters
    * Useful setter to use in builder style (eg. myKathraEnvironmentResource.parameters(List<KathraEnvironmentResourceParameter>).anotherQuickSetter(..))
    * @param List<KathraEnvironmentResourceParameter> parameters
    * @return KathraEnvironmentResource The modified KathraEnvironmentResource object
    **/
    public KathraEnvironmentResource parameters(List<KathraEnvironmentResourceParameter> parameters) {
        this.parameters = parameters;
        return this;
    }

    public KathraEnvironmentResource addParametersItem(KathraEnvironmentResourceParameter parametersItem) {
        if (this.parameters == null) {
            this.parameters = new ArrayList<KathraEnvironmentResourceParameter>();
        }
        this.parameters.add(parametersItem);
        return this;
    }

    /**
    * parameters
    * @return List<KathraEnvironmentResourceParameter> parameters
    **/
    @ApiModelProperty(value = "parameters")
    public List<KathraEnvironmentResourceParameter> getParameters() {
        return parameters;
    }

    /**
    * parameters
    **/
    public void setParameters(List<KathraEnvironmentResourceParameter> parameters) {
        this.parameters = parameters;
    }

    /**
    * Quick Set catalogResource
    * Useful setter to use in builder style (eg. myKathraEnvironmentResource.catalogResource(String).anotherQuickSetter(..))
    * @param String catalogResource
    * @return KathraEnvironmentResource The modified KathraEnvironmentResource object
    **/
    public KathraEnvironmentResource catalogResource(String catalogResource) {
        this.catalogResource = catalogResource;
        return this;
    }

    /**
    * catalogResource
    * @return String catalogResource
    **/
    @ApiModelProperty(value = "catalogResource")
    public String getCatalogResource() {
        return catalogResource;
    }

    /**
    * catalogResource
    **/
    public void setCatalogResource(String catalogResource) {
        this.catalogResource = catalogResource;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KathraEnvironmentResource kathraEnvironmentResource = (KathraEnvironmentResource) o;
        return Objects.equals(this.parameters, kathraEnvironmentResource.parameters) &&
        Objects.equals(this.catalogResource, kathraEnvironmentResource.catalogResource) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parameters, catalogResource, super.hashCode());
    }
}

