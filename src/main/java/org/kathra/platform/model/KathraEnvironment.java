/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */

package org.kathra.platform.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* KathraEnvironment
*/
@ApiModel(description = "KathraEnvironment")
public class KathraEnvironment extends KathraResource {

    protected List<KathraEnvironmentResource> resources = null;
    /**
    * status
    */
    public enum StatusEnum {
        @SerializedName("deploying")
        DEPLOYING("deploying"),

        @SerializedName("available")
        AVAILABLE("available"),

        @SerializedName("deleting")
        DELETING("deleting"),

        @SerializedName("failsafe")
        FAILSAFE("failsafe"),

        @SerializedName("error")
        ERROR("error");

        private String value;

        StatusEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static StatusEnum fromValue(String text) {
            for (StatusEnum b : StatusEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected StatusEnum status = null;

    /**
    * Quick Set resources
    * Useful setter to use in builder style (eg. myKathraEnvironment.resources(List<KathraEnvironmentResource>).anotherQuickSetter(..))
    * @param List<KathraEnvironmentResource> resources
    * @return KathraEnvironment The modified KathraEnvironment object
    **/
    public KathraEnvironment resources(List<KathraEnvironmentResource> resources) {
        this.resources = resources;
        return this;
    }

    public KathraEnvironment addResourcesItem(KathraEnvironmentResource resourcesItem) {
        if (this.resources == null) {
            this.resources = new ArrayList<KathraEnvironmentResource>();
        }
        this.resources.add(resourcesItem);
        return this;
    }

    /**
    * resources
    * @return List<KathraEnvironmentResource> resources
    **/
    @ApiModelProperty(value = "resources")
    public List<KathraEnvironmentResource> getResources() {
        return resources;
    }

    /**
    * resources
    **/
    public void setResources(List<KathraEnvironmentResource> resources) {
        this.resources = resources;
    }

    /**
    * Quick Set status
    * Useful setter to use in builder style (eg. myKathraEnvironment.status(StatusEnum).anotherQuickSetter(..))
    * @param StatusEnum status
    * @return KathraEnvironment The modified KathraEnvironment object
    **/
    public KathraEnvironment status(StatusEnum status) {
        this.status = status;
        return this;
    }

    /**
    * status
    * @return StatusEnum status
    **/
    @ApiModelProperty(value = "status")
    public StatusEnum getStatus() {
        return status;
    }

    /**
    * status
    **/
    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KathraEnvironment kathraEnvironment = (KathraEnvironment) o;
        return Objects.equals(this.resources, kathraEnvironment.resources) &&
        Objects.equals(this.status, kathraEnvironment.status) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resources, status, super.hashCode());
    }
}

