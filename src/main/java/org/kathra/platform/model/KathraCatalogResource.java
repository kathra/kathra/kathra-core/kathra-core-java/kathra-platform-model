/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */

package org.kathra.platform.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* KathraCatalogResource
*/
@ApiModel(description = "KathraCatalogResource")
public class KathraCatalogResource extends KathraResource {

    protected String version = null;
    protected String licence = null;
    protected String teaser = null;
    protected String description = null;
    protected String websiteUrl = null;
    protected String iconUrl = null;
    protected List<String> categories = null;
    protected List<KathraCatalogResourceParameter> parameters = null;

    /**
    * Quick Set version
    * Useful setter to use in builder style (eg. myKathraCatalogResource.version(String).anotherQuickSetter(..))
    * @param String version
    * @return KathraCatalogResource The modified KathraCatalogResource object
    **/
    public KathraCatalogResource version(String version) {
        this.version = version;
        return this;
    }

    /**
    * version
    * @return String version
    **/
    @ApiModelProperty(value = "version")
    public String getVersion() {
        return version;
    }

    /**
    * version
    **/
    public void setVersion(String version) {
        this.version = version;
    }

    /**
    * Quick Set licence
    * Useful setter to use in builder style (eg. myKathraCatalogResource.licence(String).anotherQuickSetter(..))
    * @param String licence
    * @return KathraCatalogResource The modified KathraCatalogResource object
    **/
    public KathraCatalogResource licence(String licence) {
        this.licence = licence;
        return this;
    }

    /**
    * licence
    * @return String licence
    **/
    @ApiModelProperty(value = "licence")
    public String getLicence() {
        return licence;
    }

    /**
    * licence
    **/
    public void setLicence(String licence) {
        this.licence = licence;
    }

    /**
    * Quick Set teaser
    * Useful setter to use in builder style (eg. myKathraCatalogResource.teaser(String).anotherQuickSetter(..))
    * @param String teaser
    * @return KathraCatalogResource The modified KathraCatalogResource object
    **/
    public KathraCatalogResource teaser(String teaser) {
        this.teaser = teaser;
        return this;
    }

    /**
    * teaser
    * @return String teaser
    **/
    @ApiModelProperty(value = "teaser")
    public String getTeaser() {
        return teaser;
    }

    /**
    * teaser
    **/
    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    /**
    * Quick Set description
    * Useful setter to use in builder style (eg. myKathraCatalogResource.description(String).anotherQuickSetter(..))
    * @param String description
    * @return KathraCatalogResource The modified KathraCatalogResource object
    **/
    public KathraCatalogResource description(String description) {
        this.description = description;
        return this;
    }

    /**
    * description
    * @return String description
    **/
    @ApiModelProperty(value = "description")
    public String getDescription() {
        return description;
    }

    /**
    * description
    **/
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * Quick Set websiteUrl
    * Useful setter to use in builder style (eg. myKathraCatalogResource.websiteUrl(String).anotherQuickSetter(..))
    * @param String websiteUrl
    * @return KathraCatalogResource The modified KathraCatalogResource object
    **/
    public KathraCatalogResource websiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
        return this;
    }

    /**
    * websiteUrl
    * @return String websiteUrl
    **/
    @ApiModelProperty(value = "websiteUrl")
    public String getWebsiteUrl() {
        return websiteUrl;
    }

    /**
    * websiteUrl
    **/
    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    /**
    * Quick Set iconUrl
    * Useful setter to use in builder style (eg. myKathraCatalogResource.iconUrl(String).anotherQuickSetter(..))
    * @param String iconUrl
    * @return KathraCatalogResource The modified KathraCatalogResource object
    **/
    public KathraCatalogResource iconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
        return this;
    }

    /**
    * iconUrl
    * @return String iconUrl
    **/
    @ApiModelProperty(value = "iconUrl")
    public String getIconUrl() {
        return iconUrl;
    }

    /**
    * iconUrl
    **/
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
    * Quick Set categories
    * Useful setter to use in builder style (eg. myKathraCatalogResource.categories(List<String>).anotherQuickSetter(..))
    * @param List<String> categories
    * @return KathraCatalogResource The modified KathraCatalogResource object
    **/
    public KathraCatalogResource categories(List<String> categories) {
        this.categories = categories;
        return this;
    }

    public KathraCatalogResource addCategoriesItem(String categoriesItem) {
        if (this.categories == null) {
            this.categories = new ArrayList<String>();
        }
        this.categories.add(categoriesItem);
        return this;
    }

    /**
    * categories
    * @return List<String> categories
    **/
    @ApiModelProperty(value = "categories")
    public List<String> getCategories() {
        return categories;
    }

    /**
    * categories
    **/
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    /**
    * Quick Set parameters
    * Useful setter to use in builder style (eg. myKathraCatalogResource.parameters(List<KathraCatalogResourceParameter>).anotherQuickSetter(..))
    * @param List<KathraCatalogResourceParameter> parameters
    * @return KathraCatalogResource The modified KathraCatalogResource object
    **/
    public KathraCatalogResource parameters(List<KathraCatalogResourceParameter> parameters) {
        this.parameters = parameters;
        return this;
    }

    public KathraCatalogResource addParametersItem(KathraCatalogResourceParameter parametersItem) {
        if (this.parameters == null) {
            this.parameters = new ArrayList<KathraCatalogResourceParameter>();
        }
        this.parameters.add(parametersItem);
        return this;
    }

    /**
    * parameters
    * @return List<KathraCatalogResourceParameter> parameters
    **/
    @ApiModelProperty(value = "parameters")
    public List<KathraCatalogResourceParameter> getParameters() {
        return parameters;
    }

    /**
    * parameters
    **/
    public void setParameters(List<KathraCatalogResourceParameter> parameters) {
        this.parameters = parameters;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KathraCatalogResource kathraCatalogResource = (KathraCatalogResource) o;
        return Objects.equals(this.version, kathraCatalogResource.version) &&
        Objects.equals(this.licence, kathraCatalogResource.licence) &&
        Objects.equals(this.teaser, kathraCatalogResource.teaser) &&
        Objects.equals(this.description, kathraCatalogResource.description) &&
        Objects.equals(this.websiteUrl, kathraCatalogResource.websiteUrl) &&
        Objects.equals(this.iconUrl, kathraCatalogResource.iconUrl) &&
        Objects.equals(this.categories, kathraCatalogResource.categories) &&
        Objects.equals(this.parameters, kathraCatalogResource.parameters) &&
        super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, licence, teaser, description, websiteUrl, iconUrl, categories, parameters, super.hashCode());
    }
}

