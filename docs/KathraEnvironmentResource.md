
# KathraEnvironmentResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parameters** | [**List&lt;KathraEnvironmentResourceParameter&gt;**](KathraEnvironmentResourceParameter.md) | parameters |  [optional]
**catalogResource** | **String** | catalogResource |  [optional]



