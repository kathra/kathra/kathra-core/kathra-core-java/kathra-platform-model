
# KathraCatalogResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** | version |  [optional]
**licence** | **String** | licence |  [optional]
**teaser** | **String** | teaser |  [optional]
**description** | **String** | description |  [optional]
**websiteUrl** | **String** | websiteUrl |  [optional]
**iconUrl** | **String** | iconUrl |  [optional]
**categories** | **List&lt;String&gt;** | categories |  [optional]
**parameters** | [**List&lt;KathraCatalogResourceParameter&gt;**](KathraCatalogResourceParameter.md) | parameters |  [optional]



