
# KathraEnvironmentResourceParameter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name |  [optional]
**value** | **String** | value |  [optional]
**type** | [**TypeEnum**](#TypeEnum) | type |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
INTEGER | &quot;integer&quot;
STRING | &quot;string&quot;
FLOAT | &quot;float&quot;
BOOLEAN | &quot;boolean&quot;



