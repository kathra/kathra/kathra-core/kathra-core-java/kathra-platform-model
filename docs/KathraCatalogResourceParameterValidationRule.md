
# KathraCatalogResourceParameterValidationRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**TypeEnum**](#TypeEnum) | type |  [optional]
**value** | **String** | value |  [optional]
**prompt** | **String** | prompt |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
EMPTY | &quot;empty&quot;
ALPHANUM | &quot;alphaNum&quot;
MINVALUE | &quot;minValue&quot;
MAXVALUE | &quot;maxValue&quot;
MINLENGTH | &quot;minLength&quot;
MAXLENGTH | &quot;maxLength&quot;



